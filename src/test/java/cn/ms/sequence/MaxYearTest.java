package cn.ms.sequence;

import java.math.BigInteger;

/**
 * MaxYearTest
 *
 * @author liruyu
 * @date 2022/3/4 10:31 上午
 */
public class MaxYearTest {

    public static void main(String[] args) {
        //41位二进制最小值
        String minTimeStampStr = "00000000000000000000000000000000000000000";
        //41位二进制最大值
        String maxTimeStampStr = "11111111111111111111111111111111111111111";
        //转10进制
        long minTimeStamp = new BigInteger(minTimeStampStr, 2).longValue();
        long maxTimeStamp = new BigInteger(maxTimeStampStr, 2).longValue();
        //一年总共多少毫秒
        long oneYearMills = 1L * 1000 * 60 * 60 * 24 * 365;
        //算出最大可以多少年
        System.out.println((maxTimeStamp - minTimeStamp) / oneYearMills);
    }

}
